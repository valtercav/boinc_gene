#include "boinc_db.h" 
#include "backend_lib.h"
#include "filesys.h"
#include "md5_file.h"
 
#include <zlib.h>
#include <dirent.h>
#include <string.h>
#include <regex.h>

int gene_stage(char * fname, bool exec = false) {
  int retval;
  char path[PATH_MAX];
  char md5_buf[128];
  double nbytes;

  SCHED_CONFIG conf;
  retval = conf.parse_file();
  if (retval) {
    log_messages.printf(MSG_CRITICAL, "gene_stage: Can't parse config.xml: %s\n", boincerror(retval));
    return retval;
  }
  if(conf.download_path(fname, path)) return(retval);

  log_messages.printf(MSG_NORMAL, "gene_stage: %s -> %s\n", fname, path);

  retval = md5_file(fname, md5_buf, nbytes, false);
  if(retval) {
    log_messages.printf(MSG_CRITICAL, "gene_stage: md5_file %s (%s)\n", fname, boincerror(retval));
    return retval;
  }
  log_messages.printf(MSG_NORMAL, "gene_stage: md5 %s %ld\n", md5_buf, (long) nbytes);

  if(exec) {
    retval = boinc_copy(fname, path);
    if (retval) {
      log_messages.printf(MSG_CRITICAL, "gene_stage: boinc_copy %s (%s)\n", fname, boincerror(retval));
      return retval;
    }
  }

  return 0;
}

int main(int argc, char** argv)  {
  char * fname;
  int retval;

  if(argc > 1) fname = argv[1];
  else return 1;
  retval = gene_stage(fname, true);
}

/*
int main(int argc, char** argv)  {
// argv parameters
  const char * input_dir;
  bool exec = false, debug = false;
//
  char path[PATH_MAX], path_gz[PATH_MAX], path_input[PATH_MAX];
  char dir[PATH_MAX] = "test.dir", regexp[256] = "_wu";
  int retval, i;
  DIR *dirp;
  struct dirent *dp;
  regex_t regex;
  
  for (i=1; i<argc; i++) {
    if (strcmp(argv[i], "--dir") == 0) {
      if (!argv[++i]) {
        log_messages.printf(MSG_CRITICAL, "gene_stage: %s requires an argument\n", argv[--i]);
        usage(argv[0]);
        exit(1);
      }
      strcpy(dir, argv[i]);
    } else if (strcmp(argv[i], "--regex") == 0) {
      if (!argv[++i]) {
        log_messages.printf(MSG_CRITICAL, "%s requires an argument\n", argv[--i]);
        usage(argv[0]);
        exit(1);
      }
      strcpy(regexp, argv[i]);
    } else if (strcmp(argv[i], "--exec") == 0) {
      exec = true;
    } else if (strcmp(argv[i], "--debug") == 0) {
      debug = true;
    } else {
      log_messages.printf(MSG_CRITICAL, "Unknown command line argument: %s\n", argv[i]);
      usage(argv[0]);
      exit(1);
    }
  }
// ---
  if(regcomp(&regex, regexp, 0)) {
    log_messages.printf(MSG_CRITICAL, "gene_stage: Could not compile regex (%s)\n", regexp);
    exit(1);    
  }

  SCHED_CONFIG conf;
  retval = conf.parse_file();
  if (retval) {
    log_messages.printf(MSG_CRITICAL, "gene_stage: Can't parse config.xml: %s\n", boincerror(retval));
    exit(retval);
  }

// copy input file in the download directory
  
  input_dir = config.project_path(dir);
  if(debug) log_messages.printf(MSG_NORMAL, "Scanning %s [%s]...\n", input_dir, regexp);

  if((dirp = opendir(input_dir)) == NULL) {
    log_messages.printf(MSG_CRITICAL, "gene_stage: Cannot open directory: %s\n", input_dir);
    exit(1);
  }
  while((dp = readdir(dirp)) != NULL) {
    if (dp->d_name[0] == '.') continue;
    retval = regexec(&regex, dp->d_name, 0, NULL, 0);
    if(retval) continue;

    if(conf.download_path(dp->d_name, path)) return(retval);

    sprintf(path_input, "%s/%s", input_dir, dp->d_name);

    strcpy(path_gz, path);
    strcat(path_gz, ".gz");
    if(debug) log_messages.printf(MSG_NORMAL, "%s\n  (copy) %s\n  (gzip) %s\n", path_input, path, path_gz);

// md5
    char md5_buf[128];
    double nbytes;
    retval = md5_file(path_input, md5_buf, nbytes, false);
    if(debug) printf("  (md5)  %s %ld\n", md5_buf, (long) nbytes);

    if(exec) {
      retval = boinc_copy(path_input , path);
      if (retval) return retval;
      retval = do_gzip(path_gz, path);
      if (retval) return retval;
    }
  }
  closedir(dirp);
  regfree(&regex);
}
*/
