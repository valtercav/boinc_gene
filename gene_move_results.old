#!/usr/bin/python

__author__ = 'Francesco Asnicar, Luca Masera, Paolo Morettin, Nadir Sella, Thomas Tolio'
__version__ = '0.04'
__date__ = '08 April 2014'

import logging
from sys import argv
from MySQLdb import connect
from subprocess import call
from glob import glob
import smtplib
from email.mime.text import MIMEText

# database "gene" configuration
GENE_HOSTNAME = 'localhost'
GENE_DATABASE = 'gene'
GENE_USERNAME = 'ugene'
GENE_PASSWORD = 'db@G3ne'
TBL_PCIM = 'pcim'
TBL_PCIM_IN_EXECUTION = 'pcim_in_execution'
TBL_PRE_ALARM = 'pre_alarm'
# TBL_USER = 'user'
# TBL_USER_PCIM = 'user_pcim'
TBL_WG_PARAMS = 'wg_params'


def send_email(subject, email_body, db_handle, db_cursor, pcim_id) :
	""" Send an e-mail to the user that submitted the pc-im job [need to be completed] """
	
	COMMASPACE = ', '

	_from = 'boincadm@gene.disi.unitn.it' # maybe should be put in the db (wg_params?)
	_to = ['nadirsella@gmail.com', 'francescoasnicar@gmail.com'] # must be removed and substitute with the following commented lines

	# retrieve the e-mail of the user that submitted the pc-im execution
	# db_handle.execute("SELECT e_mail FROM {} WHERE user_id IN (SELECT user_id FROM {} WHERE pcim_id = {})".format(TBL_USER, TBL_USER_PCIM, pcim_id))
	# db_cursor.commit()

	# if db_cursor.rowcount > 0 :
	msg = MIMEText(email_body, 'plain')
	msg['From'] = _from
	msg['To'] = COMMASPACE.join(_to)
	msg['Subject'] = subject

	try :
		smtpObj = smtplib.SMTP('localhost')
		smtpObj.sendmail(_from, _to, msg.as_string())
		smtpObj.quit()

		logging.info("E-mail sent to: \"" + COMMASPACE.join(_to) + "\" with subject: \"" + subject + "\"")
	except :
		logging.error("Unable to send e-mail \"" + subject + "\" to: \"" + COMMASPACE.join(_to) + "\"")
	# else :
	# 	logging.error("Unable to find the e-mail address for the user that submitted the pcim: \"" + pcim_id + "\".")


def main(argv) :
	# set-up a format for the logging prints in BOINC style
	ch = logging.StreamHandler()
	formatter = logging.Formatter("%(asctime)s [%(levelname)s] %(message)s")
	ch.setFormatter(formatter)
	logging.getLogger().addHandler(ch)

	# check the debug level
	#   0 : print only critical
	#   1 : print error and critical
	#   2 : print warning, error and critical
	#   3 : print info, warning, error and critical
	#   4 : print debug, info, warning, error and critical
	if len(argv) == 1 :
		try :
			debug_level = int(argv[0])
		except :
			logging.error("Invalid debug level \"" + argv[0] + "\", set default value: 2 (warning)")
			debug_level = 2
	else :
		logging.warning("No debug level found, set default value: 2 (warning)")
		debug_level = 2

	if (debug_level < 0) or (debug_level > 4) :
		logging.warning("Wrong debug level \"" + str(debug_level) + "\", set default value: 2 (warning)")
		debug_level = 2

	# set the logging level accordingly
	if debug_level == 0 :
		# set the logging level to CRTICAL
		logging.getLogger().setLevel(logging.CRTICAL)

	# set the logging level accordingly
	if debug_level == 1 :
		# set the logging level to ERROR
		logging.getLogger().setLevel(logging.ERROR)

	if debug_level == 2 :
		# set the logging level to WARNING
		logging.getLogger().setLevel(logging.WARNING)

	if debug_level == 3 :
		# set the logging level to INFO
		logging.getLogger().setLevel(logging.INFO)

	if debug_level == 4 :
		# set the logging level to DEBUG
		logging.getLogger().setLevel(logging.DEBUG)

	# connect to the MySQL database
	db = connect(host = GENE_HOSTNAME, db = GENE_DATABASE, user = GENE_USERNAME, passwd = GENE_PASSWORD)

	with db :
		cur = db.cursor()

		# query to read the pc-im currently in execution:
		#     pcim_id: the id auto-generated for the pc-im
		#     organism: the two letters that identifies the organism
		#     number_wus: number of work-units generated for the particular pc-im
		cur.execute("SELECT * FROM {}".format(TBL_PCIM_IN_EXECUTION))
		db.commit()

		# check if there are pc-im in execution
		if cur.rowcount > 0 :
			params = cur.fetchall()
			params_list = []

			for i in params :
				params_list.append((str(i[0]), str(i[1]), str(i[2]), int(i[3])))

			cur.execute("SELECT executions_path, results_path FROM {}".format(TBL_WG_PARAMS))
			db.commit()

			if cur.rowcount > 0 :
				execution_path, results_path = cur.fetchone()
			else :
				logging.critical("Cannot retrieve the executions path from the table \"" + TBL_WG_PARAMS + "\"")
				exit(1)

			execution_path= str(execution_path)
			results_path= str(results_path)

			#create the results folder
			try :
				call(['mkdir', '-p', results_path])
			except :
				logging.error("Could not create the folder: \"" + results_path + "\"")

			# cicle over all the pc-im in execution
			for pcim_id, pcim_name, organism, num_wus in params_list :
				pcim = pcim_id + '_' + organism

				# check if the results collected are equal to the number of work-units generated
				wu_lst = glob('../sample_results/' + pcim + '*')

				try :
					num_results = len(wu_lst)
				except :
					num_results = num_wus + 1
					logging.info("No results found for \"" + pcim + "\"")

				# retrieve pre-alarm info
				cur.execute("SELECT pre_alarm FROM {} WHERE pcim_id = {}".format(TBL_PRE_ALARM, pcim_id))
				db.commit()

				if cur.rowcount > 0 :
					pre_alarm = int(cur.fetchone()[0])

					# pre-alarm e-mail
					if (((num_results * 100) / num_wus) < 100) and (((num_results * 100) / num_wus) >= pre_alarm) :
						send_email('PC-IM \"' + pcim_name + '\" almost completed',
							'PC-IM \"' + pcim_name + '\" complete at ' + str((num_results * 100) / num_wus) + '%. If you are in hurry you can compute the left work-units by yourself.',
							db, cur, pcim_id)

						cur.execute("UPDATE {} SET email_pre_alarm = 1 WHERE pcim_id = {}".format(TBL_PCIM, pcim_id))
						db.commit()

				# pcim complete
				if num_results == num_wus :
					error = False

					# # move the input to the results path
					# try:
					#	call(['mv', '-f', execution_path + '/' + pcim, results_path])
					# except:
					#	error = True
					#	logging.error("Could not move the input folder: \"" + execution_path + '/' + pcim + "\"")

					# result_path = results_path + '/' + pcim + '/results/'
					result_path =  'drop/' + pcim + '/results/'


					# create the results directory
					# try :
					# 	call(['mkdir', '-p', result_path])
					# except :
					# 	error = True
					# 	logging.error("Could not create the folder: \"" + result_path + "\"")

					if not(error) :
						try :
							# move the results in its execution directory
							for r in wu_lst :
								filename = r[r.rfind('/')+1:]
								_from = '../sample_results/' + filename
								# _to = result_path + filename + '.gz'
								_to = 'boincadm.ssh@www.loa.istc.cnr.it:' + result_path + filename + '.gz'
								# call(['mv', _from, _to])
								call(['rcp', _from, _to])
								call(['rm', _from])

							send_email('PC-IM \"' + pcim_name + '\" completed',
								'PC-IM \"' + pcim_name + '\" completed and results succesfully moved to: \"' + result_path + '\".',
								db, cur, pcim_id)

							cur.execute("UPDATE {} SET in_execution = 0, executed = 1, email_sent = 1 WHERE pcim_id = {}".format(TBL_PCIM, pcim_id))
							db.commit()

							logging.info("PC-IM \"" + pcim_name + "\" completed and results succesfully moved to: \"" + result_path + "\"")
						except :
							logging.error("Could not move the results file in \"" + result_path + "\"")
				else :
					logging.info("No all results returned yet for PC-IM: \"" + pcim + "\"")
		else :
			logging.warning("No PC-IM in execution")


main(argv[1:])
