BOINC =  ../boinc-src
INSTALL = /home/boincadm/projects/test
CC = /usr/bin/g++

INCLUDE = -I$(BOINC)/ -I$(BOINC)/lib -I$(BOINC)/api -I$(BOINC)/sched -I$(BOINC)/db -I$(BOINC)/tools -I$(BOINC)/zip -I/usr/include/mysql 
CFLAGS = -DBIG_JOINS=1 -fno-strict-aliasing  -pthread  -Wall -Wextra -Wshadow -Wredundant-decls -Wdisabled-optimization -Wpointer-arith -Wstrict-aliasing -Wcast-align  -g -O3 -Wall

LDFLAGS = 
OBJECTS_V = $(BOINC)/sched/credit.o $(BOINC)/sched/validator.o $(BOINC)/sched/validate_util.o $(BOINC)/sched/validate_util2.o
OBJECTS_A = $(BOINC)/sched/assimilator.o $(BOINC)/sched/validate_util.o 

BOINCLIBS = $(BOINC)/sched/libsched.a $(BOINC)/api/libboinc_api.a $(BOINC)/lib/libboinc.a  $(BOINC)/lib/libboinc_crypt.a
LIBS = -lmysqlclient -lpthread -lz -lbz2 -lm -lrt -ldl -lcrypto -lssl 
#LOADLIBES = $(LIBS) $(BOINCLIBS)

.cpp.o:
	$(CC) -c $(INCLUDE) $(CFLAGS) $< -o $@

#gene_network_validator: gene_network_validator.o md5_file_gz.o
#	$(CXX) $(LDFLAGS) $^ $(OBJECTS_V) $(BOINCLIBS) $(LIBS) -o $@ 

gene_make_one_wu: gene_make_one_wu.o
	$(CXX) $(LDFLAGS) $^ $(BOINCLIBS) $(LIBS) -o $@ 

gene_assimilator: gene_assimilator.o 
	$(CXX) $(LDFLAGS) $^ $(OBJECTS_A) $(BOINCLIBS) $(LIBS) -o $@ 

gene_restrict_wu: gene_restrict_wu.o
	$(CXX) $(LDFLAGS) $^ $(BOINCLIBS) $(LIBS) -o $@ 

gene_restrict_wu_user: gene_restrict_wu_user.o
	$(CXX) $(LDFLAGS) $^ $(BOINCLIBS) $(LIBS) -o $@ 

gene_stage: gene_stage.o
	$(CXX) $(LDFLAGS) $^ $(BOINCLIBS) $(LIBS) -o $@ 

gene_gunbzip: gene_gunbzip.o
	$(CXX) $(LDFLAGS) $^ $(BOINCLIBS) $(LIBS) -o $@ 

gene_network_validator: gene_network_validator.o md5_file_gz.o

all: gene_network_assimilator gene_make_one_wu

install:
	cp gene_make_one_wu $(INSTALL)/bin
	cp gene_network_assimilator $(INSTALL)/bin

clean: 
	rm -f *.o gene_network_validator gene_make_one_wu gene_network_assimilator
