#include "boinc_db.h" 
#include "backend_lib.h"
#include "filesys.h"
#include "md5_file.h"
 
#include <zlib.h>
#include <dirent.h>
#include <string.h>
#include <regex.h>

int do_gzip(const char* strGZ, const char* strInput) {
    // take an input file (strInput) and turn it into a compressed file (strGZ)
    // get rid of the input file after
    FILE* fIn = boinc_fopen(strInput, "rb");
    if (!fIn)  return 1; //error
    gzFile fOut = gzopen(strGZ, "wb");
    if (!fOut) return 1; //error
    fseek(fIn, 0, SEEK_SET);  // go to the top of the files
    gzseek(fOut, 0, SEEK_SET);
    unsigned char buf[1024];
    long lRead = 0, lWrite = 0;
    while (!feof(fIn)) { // read 1KB at a time until end of file
        memset(buf, 0x00, 1024);
        lRead = 0;
        lRead = (long) fread(buf, 1, 1024, fIn);
        lWrite = (long) gzwrite(fOut, buf, lRead);
        if (lRead != lWrite) break;
    }
    gzclose(fOut);
    fclose(fIn);
    if (lRead != lWrite) return 1;  //error -- read bytes != written bytes
    // if we made it here, it compressed OK, can erase strInput and leave
//    boinc_delete_file(strInput);
    return 0;
}

void usage(char * name) {
  fprintf(stderr, "Usage %s --dir dir_input --regex reg_exp --debug --exec\n", name);
}

int main(int argc, char** argv)  {
// argv parameters
  bool exec = false, debug = false, gzip = false;
//
  char dir[PATH_MAX] = "test.dir";   	// input dir
  const char * dir_input;		// boinc-ized input dir
  char path_input[PATH_MAX];		// input file path
  char path_download[PATH_MAX]; 	// boinc download dir 
  char path_gz[PATH_MAX]; 
  char path_md5[PATH_MAX]; 
  char regexp[256] = "_wu";
  int retval, i;
  DIR *dirp;
  FILE *fp;
  struct dirent *dp;
  regex_t regex;
  
  for(i = 1; i < argc; i++) {
    if(strcmp(argv[i], "--dir") == 0) {
      if(!argv[++i]) {
        log_messages.printf(MSG_CRITICAL, "gene_stage: %s requires an argument\n", argv[--i]);
        usage(argv[0]);
        exit(1);
      }
      strcpy(dir, argv[i]);
    } else if(strcmp(argv[i], "--regex") == 0) {
      if(!argv[++i]) {
        log_messages.printf(MSG_CRITICAL, "gene_stage: %s requires an argument\n", argv[--i]);
        usage(argv[0]);
        exit(1);
      }
      strcpy(regexp, argv[i]);
    } else if(strcmp(argv[i], "--exec") == 0) {
      exec = true;
    } else if(strcmp(argv[i], "--debug") == 0) {
      debug = true;
    } else if(strcmp(argv[i], "--help") == 0) {
      usage(argv[0]);
      exit(1);
    } else {
      log_messages.printf(MSG_CRITICAL, "gene_stage: Unknown command line argument: %s\n", argv[i]);
      usage(argv[0]);
      exit(1);
    }
  }
// ---
  if(regcomp(&regex, regexp, 0)) {
    log_messages.printf(MSG_CRITICAL, "gene_stage: Could not compile regex (%s)\n", regexp);
    exit(1);    
  }

  SCHED_CONFIG conf;
  retval = conf.parse_file();
  if (retval) {
    log_messages.printf(MSG_CRITICAL, "gene_stage: Can't parse config.xml: %s\n", boincerror(retval));
    exit(retval);
  }

// copy input file in the download directory
  
  dir_input = config.project_path(dir);
  if(debug) log_messages.printf(MSG_NORMAL, "Scanning %s [%s]...\n", dir_input, regexp);

  if((dirp = opendir(dir_input)) == NULL) {
    log_messages.printf(MSG_CRITICAL, "gene_stage: Cannot open directory: %s\n", dir_input);
    exit(1);
  }
  while((dp = readdir(dirp)) != NULL) {		// loop over input directory 
    if (dp->d_name[0] == '.') continue;
    retval = regexec(&regex, dp->d_name, 0, NULL, 0);
    if(retval) continue;

    if(conf.download_path(dp->d_name, path_download)) return(retval);
    sprintf(path_input, "%s/%s", dir_input, dp->d_name);

    if(debug) log_messages.printf(MSG_NORMAL, "%s\n  (copy) %s\n", path_input, path_download);
    if(gzip) {
      strcpy(path_gz, path_download);
      strcat(path_gz, ".gz");
      if(debug) log_messages.printf(MSG_NORMAL, "  (gzip) %s\n", path_gz);
    }

// md5
    char md5_buf[128];
    double nbytes;
    retval = md5_file(path_input, md5_buf, nbytes, false);
    strcpy(path_md5, path_download);
    strcat(path_md5, ".md5");
    if(debug) printf("  (md5)  %s: %s %ld\n", path_md5, md5_buf, (long) nbytes);

    if(exec) {
      retval = boinc_copy(path_input , path_download);
      if (retval) return retval;
      if(gzip) {
        retval = do_gzip(path_gz, path_download);
        if (retval) return retval;
      }
      fp = fopen(path_md5, "w");
      fprintf(fp, "%s %ld\n", md5_buf, (long) nbytes);
      fclose(fp);
    }
  }
  closedir(dirp);
  regfree(&regex);
}
