//-------------------------------------------
//  gunbzip file if needed
//  (c) 2017 by VC
//-------------------------------------------
#include "boinc_api.h"
#include "filesys.h"

#include <string.h>
#include <bzlib.h>
#include <stdio.h>

#include <cerrno>
#include <iostream>

#define BUFLEN 32768L
#define DEBUG  1

using namespace std;

char * do_gunbzip(const char* strGZ, bool bKeep) {
  unsigned char buf[BUFLEN];
  long lRead = 0, lWrite = 0;
  FILE * fIn;
  char *p;
  int bzError;

  char * strOut = strdup(strGZ);
  if((p = strrchr(strOut, '.')) == NULL) {
    return (char *) strGZ;   // no dots (ignored)
  }
  if(strcmp(p, ".bz2") != 0) {
    return (char *) strGZ;   // not .gz (ignored)
  }
  *p = '\0';

  if(!(fIn = fopen(strGZ, "rb"))) {
    cerr << "gunbzip: fopen (r) error (" << strerror(errno) << ") [" << strGZ << "]" << endl;
    return NULL; // error
  }
  lRead = (long) fread(buf, sizeof(char), 2, fIn);
#ifdef DEBUG 
  cout << "header: " << buf[0] << " " << buf[1] << endl;
#endif
  if(buf[0] != 'B' || buf[1] != 'Z') {
#ifdef DEBUG 
  cout << "gunbzip: bad header (ignored)" << endl;
#endif
    return (char *) strGZ;  // not gzipped (ignored)
  }
  fseek(fIn, 0, SEEK_SET);
  BZFILE * bzf = BZ2_bzReadOpen(&bzError, fIn, 0, 0, NULL, 0);
  if (bzError != BZ_OK) {
    cerr << "gunbzip: BZ2_bzReadOpen: " << bzError << endl;
    return NULL;
  }

  FILE* fOut = boinc_fopen(strOut, "wb");
  if (!fOut) {
    cerr << "gunbzip: fopen (w) error (" << strerror(errno) << ")" << endl;
    return NULL; // error
  }
  while (bzError == BZ_OK) {
    lRead = BZ2_bzRead(&bzError, bzf, buf, BUFLEN);
    if (bzError == BZ_OK || bzError == BZ_STREAM_END) {
      lWrite = fwrite(buf, 1, lRead, fOut);
#ifdef DEBUG 
      cout << lRead << " " << lWrite << endl;
#endif
      if (lWrite != lRead) {
        cerr << "gunbzip: short write" << endl;
        return NULL;
      }
    }
  }
  if (bzError != BZ_STREAM_END) {
    cerr << "gunbzip: bzip error after read: " << bzError << endl;
    return NULL;
  }

  BZ2_bzReadClose(&bzError, bzf);
  fclose(fIn);
  fclose(fOut);
  if (!bKeep) boinc_delete_file(strGZ);
  return strOut;
}

#ifdef DEBUG
int main(void) {
  const char * file = "test.1.bz2";
  char * strGZ;

  boinc_init();
  if(boinc_is_standalone()) {
    strGZ = (char *) file;
  } else {
    strGZ = (char *) malloc(PATH_MAX);
    boinc_resolve_filename(file, strGZ, PATH_MAX);
  }

  printf("strGZ: %s\n", strGZ);
  strGZ = do_gunbzip(strGZ, true);
  printf("strGZ: %s\n", strGZ);
  boinc_finish(0);
}
#endif
