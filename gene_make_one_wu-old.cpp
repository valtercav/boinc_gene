#include <cstdlib> // exit, atoi, atof
#include <string> // string
#include <cstring> // string
#include <fstream> // ifstream
#include <sstream> // stringstream
#include <algorithm> //
#include "sched_util.h" // check_stop_daemons, count_unsent_results
#include "sched_msgs.h" // log_messages
#include "str_util.h"
#include "svn_version.h"
#include "boinc_db.h"
#include "error_numbers.h"
#include "backend_lib.h"
#include "parse.h"
#include "util.h"
#include "sched_config.h"

using namespace std;

/**
 *
 */
char* find_last(char* str, char sep) {
	int pos = -1, len = 0, j;
	char* ret_str = NULL;

	// search the sep char
	for (int i = 0; str[i] != '\0'; i++) {
		if (str[i] == sep) {
			pos = i + 1; // we want skip the last separator char
		}

		len++;
	}

	// the character was not found
	if (pos < 0) {
		return ret_str;
	}

	ret_str = new char[len - pos + 1];
	j = 0;

	while (str[pos] != '\0') {
		ret_str[j++] = str[pos++];
	}

	ret_str[j] = '\0';

	return ret_str;
}

/**
 * Read the rows of "in_files_path" (path_to_file) and store them in "infiles"
 * Return true if it's all ok, false otherwise.
 */
bool read_input_files(char* in_files_path, char** infiles, int index) {
	ifstream tile(in_files_path);
	string line;
	int i = index;

	// check if the file is open correctly
	if(!tile.is_open()) {
		return false;
	}

	//count the numbers of rows
	while (getline(tile, line)) {
		infiles[i] = new char[256];
		remove(line.begin(), line.end(), '\n');
		remove(line.begin(), line.end(), '\t');
		remove(line.begin(), line.end(), ' ');
		strcpy(infiles[i++], line.c_str());
	}

	tile.close();
	return true;
}

/**
 * Create one work-unit using the given parameters.
 * If all it's ok return 0, otherwise log the error and exit with 1.
 */
int main(int argc, char* argv[]) {
	DB_WORKUNIT wu;
	DB_APP app;
	int time_estimation, in_sticky_length, in_nosticky_length, replication_factor, deadline, colums, cut_results, index, retval;
	char wu_name[256], app_name[256], out_template[256], in_sticky_path[256], in_nosticky_path[256], out_file[256], buf[256], cmd[520];
	string str_in_template;
	char** infiles;
	double alpha;
	stringstream ss, in_template;

	// check the input parameters
	if (argc != 15) {
		log_messages.printf(MSG_CRITICAL, "wrong input parameters\n");
		exit(1);
	} else {
		strcpy(wu_name, argv[1]); // work-unit name
		strcpy(app_name, argv[2]); // application name
		strcpy(out_template, argv[3]); // output_template
		time_estimation = atoi(argv[4]); // #_pc_in_a_tile * graph_size * #_cols_experiments
		strcpy(in_sticky_path, argv[5]); // file that contains the sticky input files
		in_sticky_length = atoi(argv[6]); // number of rows in the in_sticky_path file
		strcpy(in_nosticky_path, argv[7]); // file that contains the no-sticky input files
		in_nosticky_length = atoi(argv[8]); // number of rows in the in_nosticky_path file
		replication_factor = atoi(argv[9]); // the number of copy of each work-unit
		deadline = atoi(argv[10]); // days of before deadline
		alpha = atof(argv[11]); // alpha to pass to the client
		strcpy(out_file, argv[12]); // the output filename to pass to the client application
		colums = atoi(argv[13]); // total number of colums through all the input files
		cut_results = atoi(argv[14]);
	}

	// alloc the input files names array
	infiles = new char*[in_sticky_length + in_nosticky_length];

	// read the input files
	if (!read_input_files(in_sticky_path, infiles, 0)) {
		log_messages.printf(MSG_CRITICAL, "can't read input files\n");
		exit(1);
	}

	if (!read_input_files(in_nosticky_path, infiles, in_sticky_length)) {
		log_messages.printf(MSG_CRITICAL, "can't read input files\n");
		exit(1);
	}

	// check the BOINC config file
	retval = config.parse_file("..");

	if (retval) {
		log_messages.printf(MSG_CRITICAL, "can't parse config.xml: %s\n", boincerror(retval));
		exit(retval);
	}

	// check the DB connection
	retval = boinc_db.open(config.db_name, config.db_host, config.db_user, config.db_passwd);

	if (retval) {
		log_messages.printf(MSG_CRITICAL, "can't open db: %s\n", boincerror(retval));
		exit(retval);
	}

	// get the app
	sprintf(buf, "where name='%s'", app_name);

	if (app.lookup(buf)) {
		log_messages.printf(MSG_CRITICAL, "can't find app\n");
		exit(1);
	}

	// make the XML input template
	index = 0;
	in_template.str(std::string()); // cleans the stringstream
	in_template << "<input_template>\n";

	for (int i = 0; i < in_sticky_length; i++) {
		in_template << "\t<file_info>\n";
		in_template << "\t\t<number>";

		in_template << index;

		in_template << "</number>\n";
		in_template << "\t\t<gzip/>\n";
		in_template << "\t\t<sticky/>\n";
		in_template << "\t\t<no_delete/>\n";
		in_template << "\t\t<report_on_rpc/>\n"; // for compatibility with sticky files
		in_template << "\t</file_info>\n";
		index++;
	}

	for (int i = 0; i < in_nosticky_length; i++) {
		in_template << "\t<file_info>\n";
		in_template << "\t\t<number>";

		in_template << index;

		in_template << "</number>\n";
		in_template << "\t\t<gzip/>\n";
		in_template << "\t</file_info>\n";
		index++;
	}

	in_template << "\t<workunit>\n";
	index = 0;

	for (int i = 0; i < in_sticky_length; i++) {
		in_template << "\t\t<file_ref>\n";
		in_template << "\t\t\t<file_number>";

		in_template << index;

		in_template << "</file_number>\n";
		in_template << "\t\t\t<open_name>";

		char* name = find_last(infiles[i], '/');
		in_template << name;
		delete name;

		in_template << "</open_name>\n";
		in_template << "\t\t</file_ref>\n";
		index++;
	}

	for (int i = 0; i < in_nosticky_length; i++) {
		in_template << "\t\t<file_ref>\n";
		in_template << "\t\t\t<file_number>";

		in_template << index;

		in_template << "</file_number>\n";
		in_template << "\t\t\t<open_name>";

		char* name = find_last(infiles[i + in_sticky_length], '/');
		in_template << name;
		delete name;

		in_template << "</open_name>\n";
		in_template << "\t\t</file_ref>\n";
		index++;
	}

	in_template << "\t\t<command_line>";

	// no-sticky files (should be):  experiments and tiles
	for (int i = 0; i < in_nosticky_length; i++) {
		char* name = find_last(infiles[i + in_sticky_length], '/');
		in_template << name;
		in_template << " ";
		delete name;
	}

	// output filename
	in_template << out_file << " ";

	// alpha
	in_template << alpha << " ";

	// number of sticky (hibridizations) files
	in_template << in_sticky_length << " ";

	// number of colums (total) through all the input (sticky) files
	in_template << colums << " ";

	// threshold under which discard the arc
	in_template << cut_results;

	in_template << "</command_line>\n";
	in_template << "\t</workunit>\n";
	in_template << "</input_template>";

	// move input file into download dir
	for (int i = 0; i < (in_sticky_length + in_nosticky_length); i++) {
		// compress the input file
		sprintf(cmd, "cd ..; bin/stage_file --gzip --copy %s", infiles[i]);

		retval = system(cmd);

		if (retval) {
			log_messages.printf(MSG_CRITICAL, "file copy: %s\n", boincerror(retval));
			exit(retval);
		}

		log_messages.printf(MSG_DEBUG, "file \"%s\" moved to download directory\n", infiles[i]);
	}

	// fill in the job parameters
	wu.clear();
	wu.appid = app.id;
	strcpy(wu.name, wu_name);
	wu.rsc_fpops_est = 1e9 * time_estimation;
	wu.rsc_fpops_bound = 1e11 * time_estimation;
	wu.rsc_memory_bound = 1e8;
	wu.rsc_disk_bound = 1e8;
	wu.delay_bound = 86400 * deadline;
	wu.min_quorum = replication_factor;
	wu.target_nresults = replication_factor;
	wu.max_error_results = replication_factor * 4;
	wu.max_total_results = replication_factor * 8;
	wu.max_success_results = replication_factor * 4;

	for (int i = 0; i < (in_sticky_length + in_nosticky_length); i++) {
		infiles[i] = find_last(infiles[i], '/');
	}

	str_in_template = in_template.str();

	// Register the job with BOINC
	return create_work(wu, /* object that represents a WU */
		str_in_template.c_str(), /* input_template */
		out_template, /* output_template */
		config.project_path(out_template), /**/
		(const char**) infiles, /* files input array */
		(in_sticky_length + in_nosticky_length), /* infiles size */
		config); /* BOINC config file */
}
