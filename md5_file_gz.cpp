// This file is part of BOINC.
// http://boinc.berkeley.edu
// Copyright (C) 2008 University of California
//
// BOINC is free software; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any later version.
//
// BOINC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with BOINC.  If not, see <http://www.gnu.org/licenses/>.

#if   defined(_WIN32) && !defined(__STDWX_H__)
#include "boinc_win.h"
#elif defined(_WIN32) && defined(__STDWX_H__)
#include "stdwx.h"
#else
#include "config.h"
#ifdef _USING_FCGI_
#include "boinc_fcgi.h"
#else
#include <cstdio>
#endif
#endif

#ifdef _WIN32
#include <wincrypt.h>
#endif

#ifdef ANDROID
#include <stdlib.h>
#endif

#include "error_numbers.h"
#include "md5.h"

#include "md5_file.h"

#include "sched_msgs.h"	// VC log_messages
#include <stdlib.h>	// VC exit

int md5_file_gz(const char* path, char* output, double& nbytes) {
    unsigned char buf[4096];
    unsigned char binout[16];
    md5_state_t state;
    int i, n;

    nbytes = 0;
#ifndef _USING_FCGI_
    FILE *f = fopen(path, "rb");
#else
    FILE *f = FCGI::fopen(path, "rb");
#endif
    if (!f) {
        fprintf(stderr, "md5_file: can't open %s\n", path);
#ifndef _USING_FCGI_
        std::perror("md5_file");
#else
        FCGI::perror("md5_file");
#endif

        return ERR_FOPEN;
    }
    md5_init(&state);
// VC
// Skip first 10 bytes (header) for gzipped files
    n = (int)fread(buf, 1, 10, f);
    if(n != 10) {
      log_messages.printf(MSG_CRITICAL, "Failure to read gzip header: %s\n", path);
      exit(1);
    }
    if(buf[0] != 0x1f || buf[1] != 0x8b || buf[2] != 0x08) {
      log_messages.printf(MSG_CRITICAL, "Not a deflated gzip file: %s\n", path);
      exit(1);
    }
// VC (end)
    while (1) {
        n = (int)fread(buf, 1, 4096, f);
        if (n<=0) break;
        nbytes += n;
        md5_append(&state, buf, n);
    }
    md5_finish(&state, binout);
    for (i=0; i<16; i++) {
        sprintf(output+2*i, "%02x", binout[i]);
    }
    output[32] = 0;
    fclose(f);
    return 0;
}
