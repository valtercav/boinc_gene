<?php
require_once('GetOpts.php');
list($errors, $options, $args) = (new GetOpts([
  'exec' => [GetOpts::SIMPLE, 'exec' ],
  'exp' => [GetOpts::VALUE, 'exp' ],
  'alpha' => [GetOpts::VALUE, 'alpha'],
  'tsize' => [GetOpts::VALUE, 'tsize'],
  'app' => [GetOpts::VALUE, 'app'],
  'app_version' => [GetOpts::VALUE, 'app_version']
]))->parse();

$default = array(
  'exp' => '2', 'alpha' => '0.05', 'tsize' => '1000',
  'app' => 'gene_pcim', 'app_version' => '1.00', 'exec' => false
);
#print_r($errors);
foreach($default as $key => $value) {
  if(empty($options[$key])) $options[$key] = $default[$key];
}
print_r($options);
echo '--------------'."\n";

# boinc parameters
$app = $options['app'];
$app_version = $options['app_version'];

# experiment parametrs
$exp_file = 'vv_experiments.csv';
$exp_path = './'.$exp_file;
$alpha = $options['alpha'];

# benchmark parameters
$npc = 5;
$tilesize = $options['tsize'];
$inp_file = basename($exp_file).'-'.$tilesize.'-t'.$npc.'.input';

# exe (benchmark application)
$exe = './pc';

# host
$h_name = gethostname();
$xml = simplexml_load_file('/var/lib/boinc-client/client_state.xml');
if($xml === false) {
  echo 'error: xml load'."\n";
  return 0;
}
$h_iops  = $xml->host_info->p_iops[0];
$h_flops = $xml->host_info->p_fpops[0];

$sql = 'INSERT INTO benchmark '. 
 '(host_name, host_iops, host_flops) VALUES '. 
 "('$h_name', '$h_iops', '$h_flops');";
echo $sql."\n";
  
# get first line of experiments
if($handle = fopen($exp_path, 'r')) {
  $line = fgets($handle);
  fclose($handle);
} else {
}

$columns = substr_count($line, ',');
$rows = rtrim(`wc -l <$exp_path`) - 1;
echo basename($exp_path);
echo ' ('.$columns.' x '.$rows.")\n";

function random_make($rows) {
  $array = range(0, $rows - 1);
  shuffle($array);
  return $array;
}

$out = fopen($inp_file, 'w');
fwrite($out, basename($exp_file)."\n");

$random = random_make($rows, 0);
shuffle($random);
for($i = 0; $i < $npc ; $i++) {
  $numbers = array_splice($random, 0, $tilesize);
  $missing = $tilesize - count($numbers);
  if($missing > 0) {
    $random = random_make($rows, 0);
    $numbers = array_merge($numbers, array_splice($random, 0, $missing));
  }
  fwrite($out, implode(' ', $numbers)."\n");
}
fclose($out);
$app_cmd = $inp_file.' '.'out '.$alpha. ' 1 '. $columns.' 0'; 
echo $exe.' '.$app_cmd,"\n";


if($options['exec'] == true) {
  $s = microtime(true);
  $exe = exec($exe.' '.$app_cmd);
  $e = microtime(true);
  printf("pc_time: %.4f\n", ($e - $s)/$npc);
}

?>
