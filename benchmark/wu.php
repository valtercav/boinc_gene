<?php
# boinc parameters
$boinc_url = '/home/boincadm/projects/test';
$application = 'gene';

/*
# find application version
$platform = 'x86_64-pc-linux-gnu';
$version = '0.10';
$app_name = 'pc';

*/
# experiment parametrs
$exp_file = 'gene_input_chaos/dream5/net3_expression_data_pc.csv';
$exp_path =  $boinc_url.'/'.$exp_file;
$alpha = 0.05;

# parameters
$npc = 100;
$tilesize = 100;
$iter = 100;

# lgn (fake)
$lgn = array(7, 77, 777);
$lgnsize = count($lgn);

# get first line of experiments
if($handle = fopen($exp_path, 'r')) {
  $line = fgets($handle);
  fclose($handle);
} else {
}

function random_make($rows, $lgn) {
  $array = range(0, $rows - 1);
#echo 'indexes     :'.count($array)."\n";
  $array = array_diff($array, $lgn);
  shuffle($array);
#echo 'indexes -lgn:'.count($array)."\n";
  return $array;
}

$columns = substr_count($line, ',');
$rows = rtrim(`wc -l <$exp_path`) - 1;
echo $columns.' x '.$rows.' ';
echo $exp_path."\n";
echo "npc = $npc, tilesize = $tilesize, iter = $iter\n";

$randomtile = $tilesize - $lgnsize;
$random = random_make($rows, $lgn);

for($j = 0; $j < $iter; $j++) {
  $output_file = time().'_wu-'.$j.'_tile.txt';
  $out = fopen($output_file, 'w');
  fwrite($out, basename($exp_file)."\n");

  for($i = 0; $i < $npc ; $i++) {
    $numbers = array_splice($random, 0, $randomtile);
#    echo $i,': ', count($numbers), ' ',$tilesize,"\n";
    if(count($numbers) < $randomtile) {
      $missing = $randomtile - count($numbers);
      echo $i,' missing ',$missing,"\n"; 
      $random = random_make($rows, $lgn);
      $numbers = array_merge($numbers, array_splice($random, 0, $missing));
    } 
# optimize ?
    $numbers = array_merge($numbers, $lgn);
    shuffle($numbers);
    fwrite($out, implode(' ', $numbers)."\n");
  }
  fclose($out);
}
#echo $output_file,' ','out ',$alpha, ' 1 ', $columns,"\n"; 
?>
