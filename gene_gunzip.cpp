//-------------------------------------------
//  gunzip file if needed
//  (c) 2017 by VC
//-------------------------------------------
#include "boinc_api.h"
#include "filesys.h"

#include <string.h>
#include <zlib.h>
#include <stdio.h>

#define BUFLEN 32768L
#define DEBUG  1

char * do_gunzip(const char* inputfile, bool bKeep) {
  unsigned char buf[BUFLEN];
  long lRead = 0, lWrite = 0;
  FILE * file;
  char *p;
  char * strGZ;

  if(boinc_is_standalone()) {
    strGZ = (char *) inputfile;
  } else {
    strGZ = (char *) malloc(PATH_MAX);
    boinc_resolve_filename(inputfile, strGZ, PATH_MAX);
  }

  char * strOut = strdup(strGZ);
  if((p = strrchr(strOut, '.')) == NULL) {
    return (char *) strGZ;   // no dots (ignored)
  }
  if(strcmp(p, ".gz") != 0) {
    return (char *) strGZ;   // not .gz (ignored)
  }
  *p = '\0';

  if(!(file = fopen(strGZ, "rb"))) {
    fprintf(stderr, "gunzip: fopen (r) error (%m) [%s]\n", strGZ);
    return NULL; // error
  }
  lRead = (long) fread(buf, sizeof(char), 2, file);
#ifdef DEBUG 
  printf("header: %x %x\n", buf[0], buf[1]);
#endif
  if(buf[0] != 0x1f || buf[1] != 0x8b) {
#ifdef DEBUG 
    printf("gunzip: bad header (ignored)\n"); 
#endif
    return (char *) strGZ;  // not gzipped (ignored)
  }
  fclose(file);

  gzFile fIn = gzopen(strGZ, "rb");
  if (!fIn) { 
    fprintf(stderr, "gunzip: gzopen error (%m)\n");
    return NULL; // error
  }

  FILE* fOut = boinc_fopen(strOut, "wb");
  if (!fOut) {
    fprintf(stderr, "gunzip: fopen (w) error (%m)\n");
    return NULL; // error
  }

  while (!gzeof(fIn)) { // read BUFLEN at a time until end 
    lRead = (long) gzread(fIn, buf, BUFLEN);
    lWrite = (long) fwrite(buf, 1, lRead, fOut);
#ifdef DEBUG 
    printf("%ld %ld\n", lRead, lWrite);
#endif
    if(lRead != lWrite) break;
  }
  gzclose(fIn);
  fclose(fOut);
  if (lRead != lWrite) {
    fprintf(stderr, "gunzip: read/write error\n");
    return NULL;
  }
  if (!bKeep) boinc_delete_file(strGZ);
  return strOut;
}

#ifdef DEBUG
int main(void) {
  const char * file = "test.1.gz";

  boinc_init();
  printf("file: %s\n", file);
  file = do_gunzip(file, true);
  printf("file: %s\n", file);
  boinc_finish(0);
}
#endif
